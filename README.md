# Color management and HDR documentation for FOSS graphics

Documentation in [this repository](https://gitlab.freedesktop.org/pq/color-and-hdr)
is intended to help with the design and implementation of color
management and HDR support on FOSS graphincs stacks, including
Mesa (EGL, Vulkan WSI), Linux (DRM KMS), Wayland (compositors and
applications), and even X11.

This is not an archive of proprietary documents like SMPTE, ITU, or VESA
specifications. All content must follow the [license](LICENSE).

## History

Originally this documentation was started to better explain how
[Wayland color management extension](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/14)
should be used and what it means.

Widening community interestests particularly in HDR prompted for moving
the documentation into this separate repository to allow a more
streamlined way of contributing to it.

For now, this project lives in a personal space, but if it gets more
traction, moving it into an independent Gitlab group is possible without
losing any Issues or MRs.

## Releases

No releases are made from this repository. Use date or the git hash to
refer to specific revisions of the contents.

## Contributing

Open Issues and Merge Requests in Gitlab as usual. Use your own forked
repository for MR branches (@pq is exempt as long as this repository is
hosted in his personal Gitlab group). Each commit must carry
Signed-off-by tag to denote that the submitter adheres to
[Developer Certificate of Origin 1.1](https://developercertificate.org/).

All merge requests need to be accepted by at least one other person with
Developer or higher access level.

Reporter level access can be given by invite without any particular
requirements.

Developer access can be given on request, provided the person is
actively participating in discussions and has contributed an accepted
MR.

Maintainer access is given on Maintainers' collective discretion.

## Conduct

This project follows
[the freedesktop.org Contributor Covenant](https://www.freedesktop.org/wiki/CodeOfConduct).

